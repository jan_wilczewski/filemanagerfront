import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ReaderService} from '../common/reader.service';
import {Customers} from '../model/customers.model';
import {Subscription} from 'rxjs/Subscription';
import {DeleteService} from '../common/delete.service';
import {UpdateService} from '../common/update.service';

@Component({
  selector: 'app-reader',
  templateUrl: './reader.component.html',
  styleUrls: ['./reader.component.css']
})
export class ReaderComponent implements OnInit{

  resetSearchResultSubscription: Subscription;

  ngOnInit(): void {
    this.resetSearchResultSubscription = this.deleteService.resetSearchResult.subscribe(
      () => {
        this.submitted = false;
        this.fileDeleted = true;
        // this.customers = null;
      }
    )
  }
  fileDeleted = false;

  fileTypes = ['.xml', '.xlsx'];
  defaultType = '';
  submitted = false;
  filterTypeName = '';
  public requestedFile = '';
  public currentFileName = '';
  public currentFileType = '';
  customers: Customers;
  filesList = [];

  // customerList = [];

  requestSent = {
    fileType: '',
    fileName: ''
  };

  @ViewChild('z') requestForm: NgForm;

  constructor(private readerService: ReaderService, private deleteService: DeleteService, private updateService: UpdateService) {
  }

  onSubmit() {

    console.log(this.requestForm.value);

    this.readerService.resetDetailFormSubject.next();
    this.readerService.resetDeleteFileButton.next();

    this.submitted = true;
    this.requestSent.fileType = this.requestForm.value.request.fileType;
    this.requestSent.fileName = this.requestForm.value.request.fileName;
    this.requestedFile = this.requestForm.value.request.fileName + this.requestForm.value.request.fileType;
    this.currentFileName = this.requestedFile;
    this.currentFileType = this.requestSent.fileType;

    this.readerService.getFile(this.requestedFile, this.requestSent.fileType).subscribe((result) => this.customers = result);

    this.requestForm.reset();

    this.updateService.updateOptionOn = false;

  }

  showFiles() {
    this.readerService.getFilesList(this.filterTypeName).subscribe((result) => this.filesList = result);
  }
}
