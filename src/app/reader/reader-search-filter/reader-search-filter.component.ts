import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Customers} from '../../model/customers.model';
import {NgForm} from '@angular/forms';
import {ReaderService} from '../../common/reader.service';
import {Customer} from '../../model/customer.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-reader-search-filter',
  templateUrl: './reader-search-filter.component.html',
  styleUrls: ['./reader-search-filter.component.css']
})
export class ReaderSearchFilter implements OnInit {

  formResetSubscription: Subscription;

  ngOnInit(): void {
    this.formResetSubscription = this.xmlService.resetDetailFormSubject.subscribe(
      () => {
        this.detailsForm.resetForm();
        this.choosedFilterName = false;
        this.choosedFilterCity = false;
      }
    )
  }

  @Input() fileNameToFilter: string;
  @Input() fileTypeToFilter: string;

  filterTypes = [{
    id: '',
    val: ''
  },
    {id: 'byName',
      val: 'Find customer by name'},
    {id: 'byCity',
      val: 'Show customers by city'}
      ];

  choosedFilterName: boolean = false;
  choosedFilterCity: boolean = false;

  submitted = false;
  filterName='';

  customer: Customer;
  customers: Customers;

  searchRequestDetails = {
    filterName:'',
    customerName:'',
    customerCity:''
  };


  @ViewChild('searchForm') detailsForm: NgForm;

  constructor(private xmlService: ReaderService) { }

  onSubmit() {
    this.submitted = true;

    this.searchRequestDetails.filterName = this.detailsForm.value.request.filterName;
    this.searchRequestDetails.customerName = this.detailsForm.value.request.customerName;
    this.searchRequestDetails.customerCity = this.detailsForm.value.request.customerCity;

    if (this.searchRequestDetails.filterName === 'byName') {
      console.log('done by name');
      this.choosedFilterName = true;
      this.choosedFilterCity = false;
      this.xmlService.getName(this.fileNameToFilter, this.searchRequestDetails.customerName, this.fileTypeToFilter).subscribe((result) => this.customer = result);
    }

    if (this.searchRequestDetails.filterName === 'byCity') {
      console.log('done by city');
      this.choosedFilterCity = true;
      this.choosedFilterName = false;
      this.xmlService.getCity(this.fileNameToFilter, this.searchRequestDetails.customerCity, this.fileTypeToFilter).subscribe((result) => this.customers = result);
    }
  }



}
