import {Component, Input, OnInit} from '@angular/core';
import {Customers} from '../model/customers.model';
import {UpdateService} from '../common/update.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  @Input() customersToModify: Customers;
  @Input() fileNameToModify: string;
  @Input() fileTypeToModify: string;

  savedText='';

  constructor(public updateService: UpdateService) { }

  ngOnInit() {
  }

  submittedButton = false;

  onUpdate() {
    this.submittedButton = true;
    this.updateService.updateOptionOn = true;
  }

  onConfirmChanges() {
    this.updateService.updateFile(this.fileNameToModify, this.customersToModify, this.fileTypeToModify).subscribe(
      value => {
        console.log('[PUT] updated Customers file successfully', value);
      }, error => {
        console.log('FAIL to update Customers file!');
      },
      () => {
        console.log('PUT Customer - now completed.');
      });

    this.savedText = 'File was saved successfully.';
  }
}
