import {Component, Input, OnInit} from '@angular/core';
import {DeleteService} from '../common/delete.service';
import {Subscription} from 'rxjs/Subscription';
import {ReaderService} from '../common/reader.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent  implements OnInit {

  buttonResetSubscription: Subscription;
  submittedButton = false;

  ngOnInit(): void {
    this.buttonResetSubscription = this.readerService.resetDeleteFileButton.subscribe(
      () => {
        this.submittedButton = false;
      }
    )
  }

  constructor(private deleteService: DeleteService, private readerService: ReaderService) { }

  @Input() fileNameToDelete: string;
  @Input() fileTypeToDelete: string;

  onDelete() {
    this.deleteService.deleteFile(this.fileNameToDelete, this.fileTypeToDelete).subscribe();
    this.deleteService.resetSearchResult.next();
    this.submittedButton = true;
  }

}
