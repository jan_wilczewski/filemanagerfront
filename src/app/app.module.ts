import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppRoutingModule} from './app.routing';


import { AppComponent } from './app.component';
import { ReaderComponent } from './reader/reader.component';
import {FormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ReaderService} from './common/reader.service';
import {WriterService} from './common/writer.service';
import {ReaderSearchFilter} from './reader/reader-search-filter/reader-search-filter.component';
import { WriterComponent } from './writer/writer.component';
import { ItemToSaveComponent } from './writer/item-to-save/item-to-save.component';
import { WriterPreviewComponent } from './writer/writer-preview/writer-preview.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import {RouterModule} from '@angular/router';
import {DeleteService} from './common/delete.service';
import { DeleteComponent } from './delete/delete.component';
import { UpdateComponent } from './update/update.component';
import {UpdateService} from './common/update.service';


@NgModule({
  declarations: [
    AppComponent,
    ReaderComponent,
    ReaderSearchFilter,
    WriterComponent,
    ItemToSaveComponent,
    WriterPreviewComponent,
    HomeComponent,
    HeaderComponent,
    DeleteComponent,
    UpdateComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    ReaderService,
    WriterService,
    DeleteService,
    UpdateService,
    HttpClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
