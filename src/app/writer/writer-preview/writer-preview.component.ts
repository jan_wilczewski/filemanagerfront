import {Component, Input, OnInit} from '@angular/core';
import {WriterService} from '../../common/writer.service';

@Component({
  selector: 'app-writer-preview',
  templateUrl: './writer-preview.component.html',
  styleUrls: ['./writer-preview.component.css']
})
export class WriterPreviewComponent {

  @Input() customersToShow: string;
  @Input() fileName: string;

  constructor(private writerService: WriterService){}

  onSaveToTheFile() {

    this.writerService.saveFileSubject.next();

  }
}
