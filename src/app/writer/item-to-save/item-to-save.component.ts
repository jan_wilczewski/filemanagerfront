import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ReaderService} from '../../common/reader.service';
import {Customer} from '../../model/customer.model';
import {Customers} from '../../model/customers.model';
import {WriterService} from '../../common/writer.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-item-to-save',
  templateUrl: './item-to-save.component.html',
  styleUrls: ['./item-to-save.component.css']
})
export class ItemToSaveComponent implements OnInit, OnDestroy {

  submitted = false;
  customer: Customer;
  customers: Customers;
  customersPreviewList: Customer[] = [];
  previewToShow = false;


  customersNewList: Customers;

  saveFileSubscription = new Subscription();

  @ViewChild('itemForm') filledItemForm: NgForm;

  @Input() fileNameToSave: string;
  // @Input() savedText: string;
  @Input() fileTypeToSave: string


  ngOnInit(): void {
    this.customersNewList = new Customers();
    this.customersNewList.customerList = [];

    this.saveFileSubscription = this.writerService.saveFileSubject.subscribe(() => this.onFinalSave());
  }

  ngOnDestroy(): void {
    this.saveFileSubscription.unsubscribe();
  }

  constructor(private writerService: WriterService) {
  }

  onAddItem() {

    this.submitted = true;
    this.customer = this.filledItemForm.value;

    this.customer.name = this.filledItemForm.value.request.customerName;
    this.customer.lastName = this.filledItemForm.value.request.customerLastName;
    this.customer.age = this.filledItemForm.value.request.customerAge;
    this.customer.city = this.filledItemForm.value.request.customerCity;
    this.customer.phone = this.filledItemForm.value.request.customerPhone;
    this.customer.email = this.filledItemForm.value.request.customerEmail;

    this.customersPreviewList.push(this.customer);
    this.filledItemForm.reset();

    this.customers = new Customers();
    this.customers.customerList = this.customersPreviewList;

    this.previewToShow = true;
    }

  onFinalSave() {
    this.writerService.saveFile(this.fileNameToSave, this.customers, this.fileTypeToSave).subscribe(
      value => {
        console.log('[POST] create Customers file successfully', value);
      }, error => {
        console.log('FAIL to create Customers file!');
      },
      () => {
        console.log('POST Customer - now completed.');
      });

    // this.savedText = 'File was saved successfully.';

    this.customersPreviewList = [];
    this.writerService.showWritingForm = false;
    this.writerService.fileSavedMessage = true;

  }
}
