import {Component, OnInit, ViewChild} from '@angular/core';
import {Customers} from '../model/customers.model';
import {ReaderService} from '../common/reader.service';
import {NgForm} from '@angular/forms';
import {Customer} from '../model/customer.model';
import {WriterService} from '../common/writer.service';

@Component({
  selector: 'app-writer',
  templateUrl: './writer.component.html',
  styleUrls: ['./writer.component.css']
})
export class WriterComponent {

  fileTypes = ['.xml', '.xlsx'];
  defaultType = '.xml';
  submitted = false;
  fileSaved = 'The file was saved succesfully.';

  // fileSavedMessage = '';

  public fileToSave = '';
  public currentFileName = '';
  public currentFileType = '';

  fileToSaveName = {
    fileType: '',
    fileName: ''
  };

  @ViewChild('writeForm') filledWriterForm: NgForm;

  constructor(private readerService: ReaderService, public writerService: WriterService) {
  }

  onSubmit() {

    this.readerService.resetDetailFormSubject.next();

    this.submitted = true;
    this.fileToSaveName.fileType = this.filledWriterForm.value.request.fileType;
    this.fileToSaveName.fileName = this.filledWriterForm.value.request.fileName;
    this.fileToSave = this.filledWriterForm.value.request.fileName + this.filledWriterForm.value.request.fileType;

    this.currentFileName = this.fileToSave;
    this.currentFileType = this.fileToSaveName.fileType;

    this.filledWriterForm.reset();

    // this.fileSavedMessage = '';

    this.writerService.showWritingForm=true;
    this.writerService.fileSavedMessage = false;
  }

  showForm(){
    return this.writerService.showWritingForm;
  }

}
