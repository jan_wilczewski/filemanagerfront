import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Customers} from '../model/customers.model';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';


@Injectable()
export class WriterService {

  private static readonly backendUrlExcel = 'http://localhost:8080/excel/customers';
  private static readonly backendUrlXml = 'http://localhost:8080/xml/customers';


  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  saveFileSubject = new Subject();

  showWritingForm = false;
  fileSavedMessage = false;

  constructor (private http: HttpClient) {}

  public saveFile(fileName: string, customers: Customers, fileType: string): Observable<any> {
    if (fileType === '.xlsx') {
      return this.http.post(WriterService.backendUrlExcel + '?file=' + fileName, customers);
    }
    if (fileType === '.xml') {
      return this.http.post(WriterService.backendUrlXml + '?file=' + fileName, customers);
    }
  }

}
