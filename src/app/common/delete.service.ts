import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Customers} from '../model/customers.model';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';


@Injectable()
export class DeleteService {

  private static readonly backendUrlExcel = 'http://localhost:8080/excel/customers?fileBasePath=ExcelFileBase&';
  private static readonly backendUrlXml = 'http://localhost:8080/xml/customers?fileBasePath=XmlFileBase&';

  resetSearchResult = new Subject();

  constructor (private http: HttpClient) {}

  public deleteFile(fileName: string, fileType: string): Observable<any> {

    if (fileType === '.xlsx') {
      return this.http.delete(DeleteService.backendUrlExcel + 'file=' + fileName);
    }
    if (fileType === '.xml') {
      return this.http.delete(DeleteService.backendUrlXml + 'file=' + fileName);
    }
  }
}
