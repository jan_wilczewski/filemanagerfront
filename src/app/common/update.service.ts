import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Customers} from '../model/customers.model';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class UpdateService {

  private static readonly backendUrlExcel = 'http://localhost:8080/excel/customers';
  private static readonly backendUrlXml = 'http://localhost:8080/xml/customers';

  private headers = new HttpHeaders({'Content-Type': 'application/json'});

  public updateOptionOn = false;

  constructor (private http: HttpClient) {}

  public updateFile(fileName: string, customers: Customers, fileType: string): Observable<any> {

    if (fileType === '.xlsx') {
      return this.http.put(UpdateService.backendUrlExcel + '?file=' + fileName, customers);
    }
    if (fileType === '.xml') {
      return this.http.put(UpdateService.backendUrlXml + '?file=' + fileName, customers);
    }

  }
}
