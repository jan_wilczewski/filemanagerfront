import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class ReaderService {

  private static readonly backendUrlExcel = 'http://localhost:8080/excel';
  private static readonly backendUrlXml = 'http://localhost:8080/xml';

  resetDetailFormSubject = new Subject();
  resetDeleteFileButton = new Subject();

  constructor(private http: HttpClient) {}

  public getFilesList(fileType: string): Observable<any> {
    if (fileType === ".xlsx") {
      return this.http.get(ReaderService.backendUrlExcel + '/file?folder=ExcelFileBase');
    }
    if (fileType === ".xml") {
      return this.http.get(ReaderService.backendUrlXml + '/file?folder=XmlFileBase');
    }
  }


  public getFile(fileName: string, fileType: string): Observable<any> {
    if (fileType === ".xlsx") {
      return this.http.get(ReaderService.backendUrlExcel + '/customers?file=' + fileName);
    }
    if (fileType === ".xml") {
      return this.http.get(ReaderService.backendUrlXml + '/customers?file=' + fileName);
    }
  }

  public getName(fileName: string, name: string, fileType: string): Observable<any> {
    if (fileType === ".xlsx"){
      return this.http.get(ReaderService.backendUrlExcel + '/customer/name?file=' + fileName + '&name=' + name);
    }
    if (fileType === ".xml") {
      return this.http.get(ReaderService.backendUrlXml + '/customer/name?file=' + fileName + '&name=' + name);
    }
  }

  public getCity(fileName: string, city: string, fileType: string): Observable<any> {
    if (fileType === ".xlsx") {
      return this.http.get(ReaderService.backendUrlExcel + '/customer/city?file=' + fileName + '&city=' + city);
    }
    if (fileType === ".xml") {
      return this.http.get(ReaderService.backendUrlXml + '/customer/city?file=' + fileName + '&city=' + city);
    }
  }
}
