import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ReaderComponent} from './reader/reader.component';
import {ReaderSearchFilter} from './reader/reader-search-filter/reader-search-filter.component';
import {WriterComponent} from './writer/writer.component';
import {ItemToSaveComponent} from './writer/item-to-save/item-to-save.component';
import {WriterPreviewComponent} from './writer/writer-preview/writer-preview.component';
import {NgModule} from '@angular/core';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full'},
  { path: 'reader', component: ReaderComponent},
  { path: 'writer', component: WriterComponent},
  ];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
