export class Customer {

  public name: string;
  public lastName: string;
  public age: number;
  public city: string;
  public phone: string;
  public email: string;
}
