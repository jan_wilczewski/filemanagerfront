import {Customer} from './customer.model';

export class Customers {

  public name: string;
  public customerList: Customer[];
}
